package stjepan.famous_scientists;
import android.util.Log;

import java.util.Random;

/**
 * Created by Stjepan on 28.9.2017..
 */

public class Quote{

    private Random random;

    public String getQuote(int id){
        return generateQuote(id);
    }
    private String[] dennisRitchie = new String[]{
            "For infrastructure technology, C will be hard to displace.",
            "UNIX is basically a simple operating system, but you have to be a genius to understand the simplicity.",
            "I fix things now and then, more often tweak HTML and make scripts to do things",
            "I'm not a person who particularly had heroes when growing up"

    };
    private String[] linusTorvalds = new String[]{
            "Software is like sex: it's better when it's free.",
            "Any program is only as good as it is useful",
            "Intelligence is the ability to avoid doing work, yet getting the work done",
            "If Microsoft ever does applications for Linux it means I've won",
    };
    private String[] donaldKnuth = new String[]{
            "Everyday life is like programming, I guess. If you love something you can put beauty into it.",
            "If you optimize everything, you will always be unhappy.",
            "Beware if bugs in the above code; I have only proved it correct, not tried it",
            "A list is only as strong as its weakest link.",
    };
    private String generateQuote(int id){
        Log.d("TAG", "u generateQuote si");
        String quote;
        switch(id){
            case R.id.ibtnDennisRitchie:
                quote = dennisRitchie[getRandom((dennisRitchie.length - 1),0)];
                break;
            case R.id.ibtnLinusTorvalds:
                quote = linusTorvalds[getRandom((linusTorvalds.length - 1),0)];
                break;
            case R.id.ibtnDonaldKnuth:
                quote = donaldKnuth[getRandom((donaldKnuth.length - 1),0)];
                break;
            default:
                quote = "";
        }
        return quote;
    }
    public int getRandom(int max, int min){
        if (random == null){
            random = new Random();
        }
        int range = max - min + 1;
        int randomNum = (int)(Math.random() * 3 + 1);
        return randomNum;
    }
}
