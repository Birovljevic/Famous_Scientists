package stjepan.famous_scientists;

import java.io.Serializable;

/**
 * Created by Stjepan on 28.9.2017..
 */

public class Scientist implements Serializable{

    private String mFirstName, mLastName, mDateOfBirth, mDateOfDeath, mCV;

    public String getName() {
        return this.mFirstName.concat(" ").concat(this.mLastName);
    }

    public String getYear() {
        if (this.mDateOfDeath.equals("")){
            return this.mDateOfBirth.concat(" - today");
        }
        else{
            return this.mDateOfBirth.concat(" - ").concat(this.mDateOfDeath);
        }
    }

    public String getmCV() {
        return mCV;
    }

    public Scientist(int id){
        switch(id){
            case R.id.ibtnDennisRitchie:
                this.mFirstName = "Dennis";
                this.mLastName = "Ritchie";
                this.mDateOfBirth = "September 9, 1941";
                this.mDateOfDeath = "October 12, 2011";
                this.mCV = "Dennis MacAlistair Ritchie was an American computer scientist. " +
                        "He created the C programming language and, with long-time colleague " +
                        "Ken Thompson, the Unix operating system. Ritchie and Thompson were awarded " +
                        "the Turing Award from the ACM in 1983, the Hamming Medal from the IEEE " +
                        "in 1990 and the National Medal of Technology from President Bill Clinton " +
                        "in 1999. Ritchie was the head of Lucent Technologies System Software " +
                        "Research Department when he retired in 2007. He was the \"R\" in K&R C, " +
                        "and commonly known by his username dmr.";
                break;
            case R.id.ibtnLinusTorvalds:
                this.mFirstName = "Linus";
                this.mLastName = "Torvalds";
                this.mDateOfBirth = "December 28, 1969";
                this.mDateOfDeath = "";
                this.mCV = "Linus Benedict Torvalds is a Finnish-American software engineer " +
                        "who is the creator, and for a long time, principal developer of the " +
                        "Linux kernel, which became the kernel for operating systems such as the " +
                        "Linux operating system, Android, and Chrome OS. He also created the " +
                        "distributed revision control system Git and the diving logging and planning " +
                        "software Subsurface. He was honored, along with Shinya Yamanaka, " +
                        "with the 2012 Millennium Technology Prize by the Technology Academy Finland " +
                        "\"in recognition of his creation of a new open source operating system for " +
                        "computers leading to the widely used Linux kernel\". He is also the " +
                        "recipient of the 2014 IEEE Computer Society Computer Pioneer Award. ";
                break;
            case R.id.ibtnDonaldKnuth:
                this.mFirstName = "Donald";
                this.mLastName = "Knuth";
                this.mDateOfBirth = "January 10, 1938";
                this.mDateOfDeath = "";
                this.mCV = "Donald Ervin Knuth is an American computer scientist, mathematician, " +
                        "and professor emeritus at Stanford University.\n" +
                        "He is the author of the multi-volume work The Art of Computer Programming. " +
                        "He contributed to the development of the rigorous analysis of the " +
                        "computational complexity of algorithms and systematized formal mathematical " +
                        "techniques for it. In the process he also popularized the asymptotic notation. " +
                        "In addition to fundamental contributions in several branches of theoretical " +
                        "computer science, Knuth is the creator of the TeX computer typesetting system, " +
                        "the related METAFONT font definition language and rendering system, and the " +
                        "Computer Modern family of typefaces.\n" +
                        "As a writer and scholar, Knuth created the WEB and CWEB computer programming " +
                        "systems designed to encourage and facilitate literate programming, " +
                        "and designed the MIX/MMIX instruction set architectures. Knuth strongly " +
                        "opposes granting software patents, having expressed his opinion to the " +
                        "United States Patent and Trademark Office and European Patent Organisation\n";
                break;
        }
    }
    public static Scientist getScientist(int id) {
        Scientist scientist = new Scientist(id);
        return scientist;}
    }

