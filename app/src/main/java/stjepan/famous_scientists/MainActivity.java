package stjepan.famous_scientists;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener, View.OnLongClickListener{

    ImageButton ibtnDennisRitchie, ibtnLinusTorvalds, ibtnDonaldKnuth;
    LinearLayout layoutDennisRitchie, layoutLinusTorvalds, layoutDonladKnuth;
    TextView tvDennisRitchieName, tvDennisRitchieYear, tvDennisRitchieCV,
             tvLinusTorvaldsName, tvLinusTorvaldsYear, tvLinusTorvaldsCV,
             tvDonaldKnuthName, tvDonaldKnuthYear, tvDonaldKnuthCV;

    Scientist dennisRitchie, linusTorvalds, donaldKnuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }
    private void initializeUI(){
        this.ibtnDennisRitchie = (ImageButton) findViewById(R.id.ibtnDennisRitchie);
        this.ibtnLinusTorvalds = (ImageButton) findViewById(R.id.ibtnLinusTorvalds);
        this.ibtnDonaldKnuth = (ImageButton) findViewById(R.id.ibtnDonaldKnuth);

        this.ibtnDennisRitchie.setOnClickListener(this);
        this.ibtnLinusTorvalds.setOnClickListener(this);
        this.ibtnDonaldKnuth.setOnClickListener(this);

        this.layoutDennisRitchie = (LinearLayout) findViewById(R.id.layoutDennisRitchie);
        this.layoutLinusTorvalds = (LinearLayout) findViewById(R.id.layoutLinusTorvalds);
        this.layoutDonladKnuth = (LinearLayout) findViewById(R.id.layoutDonaldKnuth);

        this.layoutDennisRitchie.setOnLongClickListener(this);
        this.layoutLinusTorvalds.setOnLongClickListener(this);
        this.layoutDonladKnuth.setOnLongClickListener(this);

        this.tvDennisRitchieName = (TextView) findViewById(R.id.tvDennisRitchieName);
        this.tvDennisRitchieYear = (TextView) findViewById(R.id.tvDennisRitchieYear);
        this.tvDennisRitchieCV = (TextView) findViewById(R.id.tvDennisRitchieCV);
        this.tvLinusTorvaldsName = (TextView) findViewById(R.id.tvLinusTorvaldsName);
        this.tvLinusTorvaldsYear = (TextView) findViewById(R.id.tvLinusTorvaldsYear);
        this.tvLinusTorvaldsCV = (TextView) findViewById(R.id.tvLinusTorvaldsCV);
        this.tvDonaldKnuthName = (TextView) findViewById(R.id.tvDonaldKnuthName);
        this.tvDonaldKnuthYear = (TextView) findViewById(R.id.tvDonaldKnuthYear);
        this.tvDonaldKnuthCV = (TextView) findViewById(R.id.tvDonaldKnuthCV);

        setContent();
    }
    private void setContent() {
        dennisRitchie = Scientist.getScientist(R.id.ibtnDennisRitchie);
        linusTorvalds = Scientist.getScientist(R.id.ibtnLinusTorvalds);
        donaldKnuth = Scientist.getScientist(R.id.ibtnDonaldKnuth);

        this.tvDennisRitchieName.setText(dennisRitchie.getName());
        this.tvDennisRitchieYear.setText(dennisRitchie.getYear());
        this.tvDennisRitchieCV.setText(dennisRitchie.getmCV());

        this.tvLinusTorvaldsName.setText(linusTorvalds.getName());
        this.tvLinusTorvaldsYear.setText(linusTorvalds.getYear());
        this.tvLinusTorvaldsCV.setText(linusTorvalds.getmCV());

        this.tvDonaldKnuthName.setText(donaldKnuth.getName());
        this.tvDonaldKnuthYear.setText(donaldKnuth.getYear());
        this.tvDonaldKnuthCV.setText(donaldKnuth.getmCV());
    }

    @Override
    public void onClick(View v) {
        Quote quote = new Quote();
        String quoteMsg = new String();
        switch(v.getId()){
            case R.id.ibtnDennisRitchie:
                quoteMsg = quote.getQuote(R.id.ibtnDennisRitchie);
                break;
            case R.id.ibtnLinusTorvalds:
                quoteMsg = quote.getQuote(R.id.ibtnLinusTorvalds);
                break;
            case R.id.ibtnDonaldKnuth:
                quoteMsg = quote.getQuote(R.id.ibtnDonaldKnuth);
                break;
        }
        Toast.makeText(this, quoteMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onLongClick(View v) {
        Intent intent = new Intent(this, Details.class);
        switch (v.getId()){
            case R.id.layoutDennisRitchie:
                intent.putExtra(Details.KEY_SCIENTIST, this.dennisRitchie);
                break;
            case R.id.layoutLinusTorvalds:
                intent.putExtra(Details.KEY_SCIENTIST, this.linusTorvalds);
                break;
            case R.id.layoutDonaldKnuth:
                intent.putExtra(Details.KEY_SCIENTIST, this.donaldKnuth);
                break;
        }
        startActivity(intent);
        return true;
    }
}

