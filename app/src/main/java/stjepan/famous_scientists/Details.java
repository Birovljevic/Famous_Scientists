package stjepan.famous_scientists;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Details extends Activity {

    public static final String KEY_SCIENTIST = "scientist";
    Scientist scientist;
    ImageView ivDetails;
    TextView tvDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initializeUI();

        scientist = (Scientist) getIntent().getSerializableExtra(KEY_SCIENTIST);
        setContent();
    }

    private void initializeUI(){
        this.ivDetails = (ImageView) findViewById(R.id.ivDetails);
        this.tvDetails = (TextView) findViewById(R.id.tvDetails);
    }
    private void setContent(){
        switch(scientist.getName()){
            case "Dennis Ritchie":
                this.ivDetails.setImageResource(R.drawable.dennis_ritchie_details);
                break;
            case "Linus Torvalds":
                this.ivDetails.setImageResource(R.drawable.linus_torvalds_details);
                break;
            case "Donald Knuth":
                this.ivDetails.setImageResource(R.drawable.donald_knuth_details);
                break;
        }
        this.tvDetails.setText(scientist.getmCV());
    }
    /*
    private void handleExtraData(Intent startingIntent){
        if (startingIntent.hasExtra(KEY_SCIENTIST)){
            Scientist scientist = (Scientist) startingIntent.getSerializableExtra(this.KEY_SCIENTIST);

            this.ivDetails.setImageResource(R.drawable.dennis_ritchie_details);
            this.tvDetails.setText(scientist.getmCV());
        }
    }
     */

}
